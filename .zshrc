#zsh-autocompletions
source ~/git/zsh-autocomplete/zsh-autocomplete.plugin.zsh

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
#zstyle :compinstall filename '/home/hyprnik/.zshrc'

#autoload -Uz compinit
#zstyle ':completion:*' menu select
#zmodload zsh/complist
#compinit
#comp_options+=(globaldots)
# End of lines added by compinstall




#Custom line to launch hyprland(temporary)

if [ "$(tty)" = "/dev/tty1" ]; then
	exec Hyprland
fi

#Starship prompt
eval "$(starship init zsh)"


#aliases

alias ls="ls -a"
alias vi="nvim"

#Sourcing zsh syntax highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
